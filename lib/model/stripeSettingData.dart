class StripeSettingData {
  String clientpublishableKey;
  String stripeSecret;
  String stripeKey;
  bool isEnabled;
  bool isSandboxEnabled;

  StripeSettingData({
    this.stripeKey = '',
    this.clientpublishableKey = '',
    this.stripeSecret = '',
    required this.isSandboxEnabled,
    required this.isEnabled,
  });

  factory StripeSettingData.fromJson(Map<String, dynamic> parsedJson) {
    return StripeSettingData(
      clientpublishableKey:
          'pk_test_51KNvDWCizcSWVBBzyNtzC3AqgxXZ0YxnpuGSWlL4Mh8cQKtw1QBK70T2ZLDjkNV3zJ9N4nbRUopD1EeCGowA49Bq00948ijjlL', //parsedJson['clientpublishableKey'] ?? '',
      stripeSecret: parsedJson['stripeSecret'] ?? '',
      isSandboxEnabled: parsedJson['isSandboxEnabled'],
      isEnabled: parsedJson['isEnabled'],
      stripeKey: parsedJson['stripeKey'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'stripeSecret': this.stripeSecret,
      'clientpublishableKey': this.clientpublishableKey,
      'isEnabled': this.isEnabled,
      'isSandboxEnabled': this.isSandboxEnabled,
      'stripeKey': this.stripeKey,
    };
  }
}
